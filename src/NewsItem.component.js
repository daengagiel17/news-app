import React from 'react';

export default class NewsItem extends React.Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="item" key={this.props.index}>
        <div className="item-image">
          <img alt={this.props.title} src={this.props.image} />
        </div>
        <div className="item-content">
          <div className="title">{this.props.title}</div>
          <div className="author-date">
            <span className="author">{this.props.name}</span>
            <span className="date">{this.props.date}</span>
          </div>
          <div className="description">{this.props.description}</div>
        </div>
      </div>
    );
  }
}