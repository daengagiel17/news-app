import React from 'react';
import axios from 'axios';
import './App.css';
import NewsItem from './NewsItem.component';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [],
    };
  }

  componentDidMount() {
    // call axios
    axios({
      method : 'GET',
      url : 'http://newsapi.org/v2/everything?q=bitcoin&from=2020-09-20&sortBy=publishedAt&apiKey=f114280494694a4ba9b83366b382a315'
    }).then( (res) => {
      console.info("Data :",res.data.articles);
      this.setState({
        listNews : res.data.articles
      });
    })
    .catch( (err) => {
      console.error(err)
    });
  }

  render() {
    return (
      <div className="container">
        <h2>NEWS FOR YOU</h2>
        <div>
          {this.state.listNews.map((news, index) => (
            <NewsItem
              index={index}
              image={news.urlToImage}
              title={news.title}
              name={news.author}
              date={news.publishedAt}
              description={news.description}
            />
          ))}
        </div>
      </div>
    );
  }
}
